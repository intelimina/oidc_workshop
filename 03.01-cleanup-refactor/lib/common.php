<?php
function render($path, $data) {
	extract($data);
	include dirname(__DIR__) . '/views/' . $path;
}

/**
 * redirect user to specified URL with clickthrough
 */
function redirect($url) {
	header("Location: " . $url);
	die('Click <a href="' . $url . '">here</a> if the browser does  not redirect you.');
}
