<?php
require_once 'lib/common.php';

// Example application with session

$date = date('r');

// link the client with an ID on the server
session_start();

// destroy session on request
if (!empty($_REQUEST['logout'])) {
	session_destroy();
	
	// redirect user to base URL
	redirect(config('baseURL'));
}

// validate that user is known against database
if (!empty($_REQUEST['username'])) {
	// look for user in the database
	$username = $_REQUEST['username'];
	$password = $_REQUEST['password'];
	$user = User::find($username, $password);

	if (empty($user)) {
		// render error if user not found
		render('unknown.php', [
			'date' => $date,
			'username' => $username,
		]);
		exit();
	} else {
		// save to session if user found
		$_SESSION['username'] = $username;
	}
}

if (empty($_SESSION['username'])) {
	render('unseen.php', [
		'date' => $date,
	]);
} else {
	render('seen.php', [
		'date' => $date,
		'username' => $_SESSION['username'],
	]);
}
