<?php
require_once(__DIR__ . '/../lib/common.php');
session_start();

$config = config('openid_connect');
$token_endpoint = $config['token_endpoint'];
$redirect_uri = urlencode(config('baseURL') . '/oic/local_login.php');
$scope = urlencode('openid email profile user_name');
$code = $_REQUEST['code'];
$grant_type = 'authorization_code';

$client_id = $config['client_id'];
$client_secret = $config['client_secret'];
$basic_auth = base64_encode("$client_id:$client_secret");

$ch = curl_init($token_endpoint);
curl_setopt($ch, CURLOPT_POST);
curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=$grant_type&code=$code&scope=$scope&redirect_uri=$redirect_uri" );
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
// curl_setopt($ch, CURLOPT_HEADER, ['Authorization', "Basic $basic_auth"] );
curl_setopt($ch, CURLOPT_USERPWD, "$client_id:$client_secret");

$response = curl_exec($ch);

// parse the response
$data = json_decode($response);
$id_token = $data->id_token;
$id_token_parts = explode('.', $id_token);
$id_token_header = $id_token_parts[0];
$id_token_payload = $id_token_parts[1];
$id_token_sig = $id_token_parts[2];

// read contents of id_token
$auth = json_decode(base64_urlsafe_decode($id_token_payload));

// insert the user (if it doesnt exist);
$username = $auth->email;
$user = User::find_by_username($username);
if (empty($user)) {
	$user = User::create([
		'username' => $username,
		'password' => '----openid----',
	]);
}

// populate the session
$_SESSION['username'] = $username;
redirect(config('baseURL'));
