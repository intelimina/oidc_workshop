<?php
require_once 'lib/common.php';

// Example application with session

$date = date('r');

// link the client with an ID on the server
session_start();

if (empty($_SESSION['count'])) {
	render('unseen.php', [
		'date' => $date,
	]);
} else {
	render('seen.php', [
		'date' => $date,
	]);
}

// update the 'count'
$_SESSION['count'] += 1;
