<?php
function render($path, $data) {
	extract($data);
	include dirname(__DIR__) . '/views/' . $path;
}
