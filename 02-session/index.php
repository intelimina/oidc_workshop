<?php
// Example application with session

$date = date('r');

// link the client with an ID on the server
session_start();

if (empty($_SESSION['count'])) {
	$seen = FALSE;
} else {
	$seen = TRUE;
}

//////////////////////////////////
?>
<h1>Good day!</h1>
<p>It is now <?= $date ?> today</p>

<hr />

<?php if ($seen): ?>
<p>I have seen you <?= $_SESSION['count'] ?> times today.</p>

<?php else: ?>
<p>I haven't seen you before.</p>
<?php endif; ?>

<hr />
<?php
print_r($_SESSION);

// update the 'count'
$_SESSION['count'] += 1;
