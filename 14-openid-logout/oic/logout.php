<?php
require_once(__DIR__ . '/../lib/common.php');
session_start();

$config = config('openid_connect');

// create the login redirect
$id_token = urlencode($_SESSION['openid_connect']['id_token']);
$session_state = urlencode($_SESSION['openid_connect']['session_state']);
$post_logout_uri = urlencode(config('baseURL')) . '/oic/local_logout.php';

$redirect_url = $config['end_session_endpoint'] . '?' .
			'id_token_hint=' . $id_token . '&' .
			'session_state=' . $session_state . '&' .
			'post_logout_redirect_uri=' . $post_logout_uri;

session_destroy();
redirect($redirect_url);
