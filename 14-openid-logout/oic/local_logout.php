<?php
require_once(__DIR__ . '/../lib/common.php');
session_start();
session_destroy();
redirect(config('baseURL'));
