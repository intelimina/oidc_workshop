<?php
$config = [
	'baseURL' => 'http://localhost/workshop/10.01-openid-config',

	'database' => [
		'default' => 'mysql://root@localhost/workshop'
	],

	'openid_connect' => [
		'url' => 'https://staging.sso.gov.ph',
		'client_id' => '@!FC9E.BE9C.2DFF.FBE6!0001!5D02.7306!0008!4C23.0AA0',
		'client_secret' => 'pass.123',
	]
];
