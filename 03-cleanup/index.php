<?php
require_once 'lib/common.php';

// Example application with session

$date = date('r');

// link the client with an ID on the server
session_start();

// destroy session on request
if (!empty($_REQUEST['logout'])) {
	session_destroy();
	
	// redirect user to base URL
	header('Location: ./');
	die('Click <a href="./">here</a> if the browser does not redirect you.');
}

if (empty($_SESSION['count'])) {
	render('unseen.php', [
		'date' => $date,
	]);
} else {
	render('seen.php', [
		'date' => $date,
	]);
}

// update the 'count'
$_SESSION['count'] += 1;
