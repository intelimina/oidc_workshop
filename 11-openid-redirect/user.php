<?php
require_once 'lib/common.php';

if (empty($_REQUEST['id'])) {
	exit("No user selected");
}

$id = $_REQUEST['id'];
$user = User::find($id);

if (!empty($_REQUEST['username'])) {
	$user->username = $_REQUEST['username'];
	$user->password = $_REQUEST['password'];
	$user->save();
}

render('user_show.php', [
	'user' => $user,
]);
