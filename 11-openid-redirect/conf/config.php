<?php
$config = [
	'baseURL' => 'http://localhost/workshop/11-openid-redirect',

	'database' => [
		'default' => 'mysql://root@localhost/workshop'
	],

	'openid_connect' => [
		'url' => 'https://staging.sso.gov.ph',
		'client_id' => '@!FC9E.BE9C.2DFF.FBE6!0001!5D02.7306!0008!4C23.0AA0',
		'client_secret' => 'pass.123',
		'authorization_endpoint' => 'https://staging.sso.gov.ph/oxauth/seam/resource/restv1/oxauth/authorize',
    		'token_endpoint' => 'https://staging.sso.gov.ph/oxauth/seam/resource/restv1/oxauth/token',
    		'userinfo_endpoint' => 'https://staging.sso.gov.ph/oxauth/seam/resource/restv1/oxauth/userinfo',
    		'end_session_endpoint' => 'https://staging.sso.gov.ph/oxauth/seam/resource/restv1/oxauth/end_session'
	]
];
