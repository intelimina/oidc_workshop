<?php
require_once(__DIR__ . '/../lib/common.php');

$config = config('openid_connect');

// create the login redirect
$response_type = 'code';
$redirect_uri = urlencode(config('baseURL') . '/oic/local_login.php');
$scope = urlencode('openid email profile user_name');
$client_id = urlencode($config['client_id']);
$state = urlencode(base64_encode(date('r')));
$nonce = urlencode(base64_encode(date('r')));

$redirect_url = $config['authorization_endpoint'] . '?' .
			'response_type=' . $response_type . '&' .
			'redirect_uri=' . $redirect_uri . '&' .
			'scope=' . $scope . '&' .
			'client_id=' . $client_id . '&' .
			'state=' . $state . '&' .
			'nonce=' . $nonce;

redirect($redirect_url);
