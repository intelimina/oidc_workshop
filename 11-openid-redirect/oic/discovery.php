<?php
require_once(__DIR__ . '/../lib/common.php');

$client_config = config('openid_connect');
$discovery_url = $client_config['url'] . '/.well-known/openid-configuration';

$ch = curl_init($discovery_url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
$response = curl_exec($ch);

print_r(json_decode($response));
