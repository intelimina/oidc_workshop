<table>
	<thead>
		<tr>
			<th>ID</th>
			<th>Username</th>
			<th>Password</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($users as $user): ?>
		<tr>
			<td><?= $user->id ?></td>
			<td><?= $user->username ?></td>
			<td><?= $user->password ?></td>
		</tr>
<?php endforeach; ?>
	</tbody>
</table>
