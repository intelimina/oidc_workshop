<?php
require_once 'lib/common.php';

$users = User::all();

render('user_list.php', ['users' => $users]);
