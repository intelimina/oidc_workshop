<?php
require_once 'lib/common.php';

// Example application with session

$date = date('r');

// link the client with an ID on the server
session_start();

// destroy session on request
if (!empty($_REQUEST['logout'])) {
	session_destroy();
	
	// redirect user to base URL
	redirect(config('baseURL'));
}

// add username to session
if (!empty($_REQUEST['username'])) {
	$_SESSION['username'] = $_REQUEST['username'];
}

if (empty($_SESSION['username'])) {
	render('unseen.php', [
		'date' => $date,
	]);
} else {
	render('seen.php', [
		'date' => $date,
		'username' => $_SESSION['username'],
	]);
}
