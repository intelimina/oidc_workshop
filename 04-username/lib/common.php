<?php
/**
 * show a view on the page
 */
function render($path, $data) {
	extract($data);
	include dirname(__DIR__) . '/views/' . $path;
}

/**
 * redirect user to specified URL with clickthrough
 */
function redirect($url) {
	header("Location: " . $url);
	die('Click <a href="' . $url . '">here</a> if the browser does  not redirect you.');
}

/**
 * get a configuration variable
 */
function config($name) {
	require dirname(__DIR__) . '/conf/config.php';
	return $config[$name];
}
