<?php
require_once(__DIR__ . '/php-activerecord/ActiveRecord.php');
require_once(__DIR__ . '/User.php');

/**
 * show a view on the page
 */
function render($path, $data) {
	extract($data);
	include dirname(__DIR__) . '/views/' . $path;
}

/**
 * redirect user to specified URL with clickthrough
 */
function redirect($url) {
	header("Location: " . $url);
	die('Click <a href="' . $url . '">here</a> if the browser does  not redirect you.');
}

/**
 * get a configuration variable
 */
function config($name) {
	require dirname(__DIR__) . '/conf/config.php';
	return $config[$name];
}

/** initialize activerecord **/
$ar_config = ActiveRecord\Config::instance();
$ar_config->set_model_directory(__DIR__);
$ar_config->set_connections(config('database'));
$ar_config->set_default_connection('default');
