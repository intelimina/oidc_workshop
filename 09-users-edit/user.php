<?php
require_once 'lib/common.php';

if (empty($_REQUEST['id'])) {
	exit("No user selected");
}

$id = $_REQUEST['id'];
$user = User::find($id);

render('user_show.php', [
	'user' => $user,
]);
