<?php
require_once 'lib/common.php';

if (!empty($_REQUEST['username'])) {
	$username = $_REQUEST['username'];
	$password = $_REQUEST['password'];
	$user = User::create([
		'username' => $username,
		'password' => $password,
	]);

	redirect(config('baseURL'));
}

$users = User::all();

render('user_list.php', ['users' => $users]);
